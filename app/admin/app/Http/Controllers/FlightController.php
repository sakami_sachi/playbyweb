<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class FlightController extends Controller
{
    public function inputSettingAction()
    {
        return view('flight.input');
    }

    public function confirmSettingAction()
    {
        return view('flight.confirm');
    }

    public function executeAction()
    {
        $appDirectory = dirname(__FILE__).'/../..';
        chdir($appDirectory);

        // メイン処理起動
        exec("php business/flightBatch.php");

        return redirect('flight/resultStartFlight');
    }

    public function resultStartFlightAction()
    {
        return view('flight.result');
    }
}