<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('flight/input', 'FlightController@inputSettingAction');

Route::get('flight/confirm', 'FlightController@confirmSettingAction');

Route::get('flight/execute', 'FlightController@executeAction');

Route::get('flight/resultStartFlight', 'FlightController@resultStartFlightAction');